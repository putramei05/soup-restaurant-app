import { Box } from "@mui/system";
import { Route, Routes, useLocation } from "react-router-dom";
import PrivateAdmin from "./_middleware/PrivateAdmin";
import PrivateRoute from "./_middleware/PrivateRoute";
import PublicRoute from "./_middleware/PublicRoute";
import Login from "./components/Login";
import Navbar from "./components/Navbar";
import NewPassword from "./components/NewPassword";
import Register from "./components/Register";
import ResetPassword from "./components/ResetPassword";
import SuccessInvoice from "./components/SuccessInvoice";
import SuccessPurchase from "./components/SuccessPurchase";
import { AuthProvider } from "./contexts/AuthContext";
import { CartProvider } from "./contexts/CartContext";
import DashboardAdmin from "./pages/Admin/DashboardAdmin";
import Checkout from "./pages/Checkout";
import InvoiceDetailContent from "./pages/Admin/InvoiceDetailContent";
import Course from "./pages/Course";
import DetailsCourse from "./pages/DetailsCourse";
import Invoice from "./pages/Invoice";
import InvoiceDetail from "./pages/InvoiceDetail";
import LandingPage from "./pages/LandingPage";
import MyClass from "./pages/MyClass";
import "./styles/App.css";

function App() {
  const location = useLocation();
  const shouldShowNavbar =
    location.pathname !== "/dashboardadmin" &&
    !location.pathname.startsWith("/dashboardadmin/invoicedetail/");

  return (
    <Box>
      <AuthProvider>
        {shouldShowNavbar && <Navbar />}
        <CartProvider>
          <Routes>
            <Route path="/" element={<LandingPage />} />
            <Route element={<PublicRoute />}>
              <Route path="/login" element={<Login />} />
              <Route path="/register" element={<Register />} />
              <Route path="/reset" element={<ResetPassword />} />
              <Route path="/success" element={<SuccessPurchase />} />
              <Route path="/confirmpassword" element={<NewPassword />} />
            </Route>
            <Route element={<PrivateRoute />}>
              <Route path="/successinvoice" element={<SuccessInvoice />} />
              <Route path="/course/:id" element={<Course />} />
              <Route path="/invoice" element={<Invoice />} />
              <Route path="/invoicedetail/:id" element={<InvoiceDetail />} />
              <Route path="/detailcourse/:id" element={<DetailsCourse />} />
              <Route path="/checkout" element={<Checkout />} />
              <Route path="/myclass" element={<MyClass />} />
            </Route>
            <Route element={<PrivateAdmin />}>
              <Route path="/dashboardadmin" element={<DashboardAdmin />} />
              <Route path="/dashboardadmin/invoicedetail/:id" element={<InvoiceDetailContent />} />
            </Route>
          </Routes>
        </CartProvider>
      </AuthProvider>
    </Box>
  );
}

export default App;
