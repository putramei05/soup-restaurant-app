import React from "react";
import { Navigate, Outlet } from "react-router-dom";

const PrivateRoute = () => {
  const cookies = document.cookie.split("; ").reduce((acc, cookie) => {
    const [name, value] = cookie.split("=");
    acc[name] = value;
    return acc;
  }, {});
  if (cookies.role === "1") {
    return <Outlet />;
  } else {
    return <Navigate to="/dashboardadmin" />;
  }
};

export default PrivateRoute;
