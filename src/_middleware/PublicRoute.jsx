import React from "react";
import { Navigate, Outlet } from "react-router-dom";

const PublicRoute = () => {
  const token = document.cookie;
  return token ? <Navigate to="/" /> : <Outlet />;
};

export default PublicRoute;
