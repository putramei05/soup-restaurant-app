import * as React from 'react';
import { styled } from '@mui/material/styles';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import TablePagination from '@mui/material/TablePagination';
import Paper from '@mui/material/Paper';
import { Button } from '@mui/material';
import { Link } from 'react-router-dom';
import useFetchData from '../../hooks/useFetchData';

const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
        backgroundColor: '#5B4947',
        color: theme.palette.common.white,
    },
    [`&.${tableCellClasses.body}`]: {
        fontSize: 14,
    },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
    '&:nth-of-type(even)': {
        backgroundColor: 'rgba(91, 73, 71, 0.2)',
    },
    // hide last border
    '&:last-child td, &:last-child th': {
        border: 0,
    },
}));


export default function InvoiceTableAdmin() {

    const userId = 2;

    const { data: data1, error: error1 } = useFetchData(`${process.env.REACT_APP_BASE_URL}/api/invoice/get`);

    // console.log(data1);
    return (
        <TableContainer component={Paper}>
            <Table sx={{ minWidth: 700 }} aria-label="customized table">
                <TableHead sx={{
                    width: '1140px',
                    height: '60px',
                    padding: '20px, 20px, 20px, 0px',
                    gap: '24px'
                }}>
                    <TableRow>
                        <StyledTableCell sx={{
                            fontWeight: 600,
                            fontFamily: 'Montserrat',
                            fontSize: '16px',
                            lineHeight: '19.5px',
                            color: '#FFFFFF'
                        }}>No</StyledTableCell>
                        <StyledTableCell align="center" sx={{
                            fontWeight: 600,
                            fontFamily: 'Montserrat',
                            fontSize: '16px',
                            lineHeight: '19.5px',
                            color: '#FFFFFF'
                        }}>No. Invoice</StyledTableCell>
                        <StyledTableCell align="center" sx={{
                            fontWeight: 600,
                            fontFamily: 'Montserrat',
                            fontSize: '16px',
                            lineHeight: '19.5px',
                            color: '#FFFFFF'
                        }}>Date</StyledTableCell>
                        <StyledTableCell align="center" sx={{
                            fontWeight: 600,
                            fontFamily: 'Montserrat',
                            fontSize: '16px',
                            lineHeight: '19.5px',
                            color: '#FFFFFF'
                        }}>Total Course</StyledTableCell>
                        <StyledTableCell align="center" sx={{
                            fontWeight: 600,
                            fontFamily: 'Montserrat',
                            fontSize: '16px',
                            lineHeight: '19.5px',
                            color: '#FFFFFF'
                        }}>Total Price</StyledTableCell>
                        <StyledTableCell align="center" sx={{
                            fontWeight: 600,
                            fontFamily: 'Montserrat',
                            fontSize: '16px',
                            lineHeight: '19.5px',
                            color: '#FFFFFF'
                        }}>Action</StyledTableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {data1.map((row, index) => {
                        const inputDate = new Date(row.invoiceDate);

                        const options = { year: 'numeric', month: 'long', day: 'numeric' };
                        const formattedDate = inputDate.toLocaleDateString('en-US', options);

                        // console.log(formattedDate);
                        return (
                            <StyledTableRow key={row.invoiceId}>
                                <StyledTableCell component="th" scope="row" sx={{
                                    fontWeight: 500,
                                    fontFamily: 'Montserrat',
                                    fontSize: '16px',
                                    lineHeight: '19.5px',
                                    color: '#4F4F4F'
                                }}>
                                    {index + 1}
                                </StyledTableCell>
                                <StyledTableCell align="center" sx={{
                                    fontWeight: 500,
                                    fontFamily: 'Montserrat',
                                    fontSize: '16px',
                                    lineHeight: '19.5px',
                                    color: '#4F4F4F'
                                }}>{row.invoiceName}</StyledTableCell>
                                <StyledTableCell align="center" sx={{
                                    fontWeight: 500,
                                    fontFamily: 'Montserrat',
                                    fontSize: '16px',
                                    lineHeight: '19.5px',
                                    color: '#4F4F4F'
                                }}>{formattedDate}</StyledTableCell>
                                <StyledTableCell align="center" sx={{
                                    fontWeight: 500,
                                    fontFamily: 'Montserrat',
                                    fontSize: '16px',
                                    lineHeight: '19.5px',
                                    color: '#4F4F4F'
                                }}>{row.totalCourse}</StyledTableCell>
                                <StyledTableCell align="center" sx={{
                                    fontWeight: 500,
                                    fontFamily: 'Montserrat',
                                    fontSize: '16px',
                                    lineHeight: '19.5px',
                                    color: '#4F4F4F'
                                }}>{row.totalPrice}</StyledTableCell>
                                <StyledTableCell align="center"><Button
                                    sx={{
                                        padding: "10px",
                                        background: "#FABC1D",
                                        color: "#5B4947",
                                        borderRadius: "8px",
                                        width: "180px",
                                        height: '37px',
                                        fontWeight: 700,
                                        fontFamily: 'Montserrat',
                                        fontSize: '14px',
                                        lineHeight: '17.07px',
                                        textTransform: 'capitalize'
                                    }}
                                    component={Link} to={`/dashboardadmin/invoicedetail/${row.invoiceId}`}
                                >Details</Button></StyledTableCell>
                            </StyledTableRow>
                        )
                    })}
                </TableBody>
            </Table>
        </TableContainer>
    );
}