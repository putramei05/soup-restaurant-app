import {useState, useEffect} from 'react';
import { styled } from '@mui/material/styles';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import {
    Button,
    Dialog,
    DialogTitle,
    DialogContent,
    DialogActions,
    TextField,
    Box,
    Typography
} from '@mui/material';
import axios from 'axios';

const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
        backgroundColor: '#5B4947',
        color: theme.palette.common.white,
    },
    [`&.${tableCellClasses.body}`]: {
        fontSize: 14,
    },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
    '&:nth-of-type(even)': {
        backgroundColor: 'rgba(91, 73, 71, 0.2)',
    },
    // hide last border
    '&:last-child td, &:last-child th': {
        border: 0,
    },
}));


export default function UserTableAdmin() {
    const [usersData, setUsersData] = useState([]);
    const [selectedUser, setSelectedUser] = useState(null);
    const [isDialogOpen, setIsDialogOpen] = useState(false);

    useEffect(() => {
        // Fetch users data from your API here
        const fetchData = async () => {
            try {
                const response = await axios.get(`${process.env.REACT_APP_BASE_URL}/api/admin/getuser`);
                setUsersData(response.data);
            } catch (error) {
                console.error('Error fetching user data:', error);
            }
        };
        fetchData();
    }, []);

    const handleInputChange = (e) => {
        const { name, type } = e.target;
        let value;

        if (type === 'checkbox' || type === 'boolean') {
            value = e.target.value;
        } else if (type === 'radio') {
            if (name === 'userRole') {
                value = parseInt(e.target.value);
            } else if (name === 'isActive') {
                value = e.target.value === 'true';
            }
        } else {
            value = e.target.value;
        }

        const updatedUser = { ...selectedUser, [name]: value };
        setSelectedUser(updatedUser);
    };

    const handleSave = async () => {
        try {
            const editUrl = `${process.env.REACT_APP_BASE_URL}/api/admin/edituser/${selectedUser.userId}`;
            const response = await axios.put(editUrl, selectedUser);
            console.log('User edited successfully:', response.data);
            setIsDialogOpen(false); // Close the dialog after saving
        } catch (error) {
            console.error('Error editing user:', error);
        }
    };

    // console.log(selectedUser);
    return (
        <TableContainer component={Paper}>
            <Table sx={{ minWidth: 700 }} aria-label="customized table">
                <TableHead sx={{
                    width: '1140px',
                    height: '60px',
                    padding: '20px, 20px, 20px, 0px',
                    gap: '24px'
                }}>
                    <TableRow>
                        <StyledTableCell sx={{
                            fontWeight: 600,
                            fontFamily: 'Montserrat',
                            fontSize: '16px',
                            lineHeight: '19.5px',
                            color: '#FFFFFF'
                        }}>No</StyledTableCell>
                        <StyledTableCell align="center" sx={{
                            fontWeight: 600,
                            fontFamily: 'Montserrat',
                            fontSize: '16px',
                            lineHeight: '19.5px',
                            color: '#FFFFFF'
                        }}>Nama</StyledTableCell>
                        <StyledTableCell align="center" sx={{
                            fontWeight: 600,
                            fontFamily: 'Montserrat',
                            fontSize: '16px',
                            lineHeight: '19.5px',
                            color: '#FFFFFF'
                        }}>Email</StyledTableCell>
                        <StyledTableCell align="center" sx={{
                            fontWeight: 600,
                            fontFamily: 'Montserrat',
                            fontSize: '16px',
                            lineHeight: '19.5px',
                            color: '#FFFFFF'
                        }}>Role</StyledTableCell>
                        <StyledTableCell align="center" sx={{
                            fontWeight: 600,
                            fontFamily: 'Montserrat',
                            fontSize: '16px',
                            lineHeight: '19.5px',
                            color: '#FFFFFF'
                        }}>Status</StyledTableCell>
                        <StyledTableCell align="center" sx={{
                            fontWeight: 600,
                            fontFamily: 'Montserrat',
                            fontSize: '16px',
                            lineHeight: '19.5px',
                            color: '#FFFFFF'
                        }}>Action</StyledTableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {usersData.map((row, index) => {
                        let  role = ''
                        let status = ''
                        
                        if (row.userRole === 1) {
                            role = 'Admin'
                        } else {
                            role = 'User'
                        }

                        if (row.isActive) {
                            status = 'Active'
                        } else {
                            status = 'Inactive'
                        }

                        return (
                            <StyledTableRow key={row.userId}>
                                <StyledTableCell component="th" scope="row" sx={{
                                    fontWeight: 500,
                                    fontFamily: 'Montserrat',
                                    fontSize: '16px',
                                    lineHeight: '19.5px',
                                    color: '#4F4F4F'
                                }}>
                                    {index + 1}
                                </StyledTableCell>
                                <StyledTableCell align="center" sx={{
                                    fontWeight: 500,
                                    fontFamily: 'Montserrat',
                                    fontSize: '16px',
                                    lineHeight: '19.5px',
                                    color: '#4F4F4F'
                                }}>{row.userName}</StyledTableCell>
                                <StyledTableCell align="center" sx={{
                                    fontWeight: 500,
                                    fontFamily: 'Montserrat',
                                    fontSize: '16px',
                                    lineHeight: '19.5px',
                                    color: '#4F4F4F'
                                }}>{row.email}</StyledTableCell>
                                <StyledTableCell align="center" sx={{
                                    fontWeight: 500,
                                    fontFamily: 'Montserrat',
                                    fontSize: '16px',
                                    lineHeight: '19.5px',
                                    color: '#4F4F4F'
                                }}>{role}</StyledTableCell>
                                <StyledTableCell align="center" sx={{
                                    fontWeight: 500,
                                    fontFamily: 'Montserrat',
                                    fontSize: '16px',
                                    lineHeight: '19.5px',
                                    color: '#4F4F4F'
                                }}>{status}</StyledTableCell>
                                <StyledTableCell align="center"><Button
                                    sx={{
                                        padding: "10px",
                                        background: "#FABC1D",
                                        color: "#5B4947",
                                        borderRadius: "8px",
                                        width: "180px",
                                        height: '37px',
                                        fontWeight: 700,
                                        fontFamily: 'Montserrat',
                                        fontSize: '14px',
                                        lineHeight: '17.07px',
                                        textTransform: 'capitalize'
                                    }}
                                    onClick={() => {
                                        setSelectedUser(row);
                                        setIsDialogOpen(true);
                                    }}
                                >Edit</Button></StyledTableCell>
                            </StyledTableRow>
                        )
                    })}
                </TableBody>
            </Table>
            {selectedUser && (
                <Dialog open={isDialogOpen} onClose={() => setIsDialogOpen(false)}>
                    <DialogTitle>Edit User</DialogTitle>
                    <DialogContent>
                        <Box display= 'flex' flexDirection= 'column'>
                            <TextField
                                label="Username"
                                value={selectedUser.userName}
                                onChange={handleInputChange}
                                name="userName"
                                sx={{marginTop: '10px'}}
                            />
                            <TextField
                                label="Email"
                                value={selectedUser.email}
                                onChange={handleInputChange}
                                name="email"
                                sx={{ marginTop: '10px' }}
                            />
                            <TextField
                                type='password'
                                label="Password"
                                value={selectedUser.password}
                                onChange={handleInputChange}
                                name="password"
                                sx={{ marginTop: '10px' }}
                            />
                            <div>
                                <Typography id="modal-modal-description" sx={{ mt: 1 }}>
                                    Role
                                </Typography>
                                <div style={{ marginTop: '5px' }}>
                                    <RadioGroup
                                        row
                                        aria-labelledby="demo-row-radio-buttons-group-label"
                                        name="userRole"
                                        value={selectedUser.userRole}
                                        onChange={handleInputChange}
                                    >
                                        <FormControlLabel value={1} control={<Radio />} label="Admin" />
                                        <FormControlLabel value={2} control={<Radio />} label="User" />
                                    </RadioGroup>
                                </div>
                            </div>
                            <div>
                                <Typography id="modal-modal-description" sx={{ mt: 1 }}>
                                    Status
                                </Typography>
                                <div style={{ marginTop: '5px' }}>
                                    <RadioGroup
                                        row
                                        aria-labelledby="demo-row-radio-buttons-group-label"
                                        name="isActive" // Field name in selectedUser object
                                        value={selectedUser.isActive} // Convert to string for RadioGroup
                                        onChange={handleInputChange}
                                    >
                                        <FormControlLabel value={true} control={<Radio />} label="Active" />
                                        <FormControlLabel value={false} control={<Radio />} label="Inactive" />
                                    </RadioGroup>
                                </div>
                            </div>
                        </Box>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={() => setIsDialogOpen(false)}>Cancel</Button>
                        <Button onClick={handleSave}>Save</Button>
                    </DialogActions>
                </Dialog>
            )}
        </TableContainer>
    );
}