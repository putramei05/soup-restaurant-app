import { Box, Typography, Grid, Avatar } from "@mui/material";
import CallIcon from "@mui/icons-material/Call";
import InstagramIcon from "@mui/icons-material/Instagram";
import YouTubeIcon from "@mui/icons-material/YouTube";
import TelegramIcon from "@mui/icons-material/Telegram";
import EmailIcon from "@mui/icons-material/Email";
import styled from "@emotion/styled";
import React from "react";

const Footer = () => {
  const IconBox = styled(Box)`
    display: flex;
    align-items: center;
    margin-top: 8px;
    flex-wrap: wrap;
  `;

  const CustomFooter = styled(Box)`
    display: flex;
    align-items: flex-start;
    padding: 24px 95px;
    justify-content: center;
    width: 100%;
    background: #5b4947;
    @media (max-width: 768px) {
      flex-direction: column;
      padding: 24px;
    }
  `;

  const SocialMediaIcon = styled(Avatar)`
    background: #fabc1d;
    margin: 2px 8px;
  `;

  const listItems = [
    "Asian",
    "Cold Drink",
    "Cookies",
    "Dessert",
    "Eastern",
    "Hot Drink",
    "Junk Food",
    "Western",
    // Add more items as needed
  ];

  return (
    <CustomFooter>
      <Box
        sx={{
          display: "column",
          // border: "1px solid white",
          alignItems: "flex-start",
          width: "350px",
          marginRight: "80px",
        }}
      >
        <Typography
          sx={{
            color: "#FABC1D",
            marginBottom: "8px",
            fontFamily: "Montserrat",
          }}
        >
          About Us
        </Typography>
        <Typography
          sx={{
            color: "#FFF",
            textAlign: "justify",
            fontSize: "14px",
            fontWeight: 400,
            fontFamily: "Montserrat",
            lineHeight: "21px",
          }}
        >
          Our name is Putra Muhammad Ferdiansyah and Bagus Setiawan
        </Typography>
      </Box>
      <Box
        sx={{
          display: "column",
          // border: "1px solid white",
          alignItems: "flex-start",
          width: "228px",
          marginRight: "80px",
        }}
      >
        <Typography
          sx={{
            color: "#FABC1D",
            marginBottom: "8px",
            fontFamily: "Montserrat",
          }}
        >
          Product
        </Typography>
        <Grid container spacing={1}>
          {listItems.map((item, index) => (
            <Grid
              item
              xl={6}
              key={index}
              sx={{
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <Typography
                variant="body1"
                component="ul"
                sx={{
                  whiteSpace: "none",
                  width: "76px",
                  color: "#FFF",
                  textAlign: "justify",
                  fontSize: "14px",
                  fontWeight: 400,
                  fontFamily: "Montserrat",
                  lineHeight: "21px",
                  mx: 1,
                }}
              >
                <li>{item}</li>
              </Typography>
            </Grid>
          ))}
        </Grid>
      </Box>
      <Box
        sx={{
          display: "column",
          // border: "1px solid white",
          alignItems: "flex-start",
          width: "350px",
        }}
      >
        <Typography
          sx={{
            color: "#FABC1D",
            marginBottom: "8px",
            fontFamily: "Montserrat",
          }}
        >
          Address
        </Typography>
        <Typography
          sx={{
            color: "#FFF",
            textAlign: "justify",
            fontSize: "14px",
            fontWeight: 400,
            fontFamily: "Montserrat",
            lineHeight: "21px",
          }}
        >
          Sed ut perspiciatis unde omnis iste natus error sit voluptatem
          accusantium doloremque.
        </Typography>
        <Typography
          sx={{
            color: "#FABC1D",
            marginBottom: "8px",
            marginTop: "16px",
            fontFamily: "Montserrat",
          }}
        >
          Contact Us
        </Typography>
        <IconBox>
          <SocialMediaIcon>
            <CallIcon sx={{ color: "#5B4947" }} />
          </SocialMediaIcon>
          <SocialMediaIcon>
            <InstagramIcon sx={{ color: "#5B4947" }} />
          </SocialMediaIcon>
          <SocialMediaIcon>
            <YouTubeIcon sx={{ color: "#5B4947" }} />
          </SocialMediaIcon>
          <SocialMediaIcon>
            <TelegramIcon sx={{ color: "#5B4947" }} />
          </SocialMediaIcon>
          <SocialMediaIcon>
            <EmailIcon sx={{ color: "#5B4947" }} />
          </SocialMediaIcon>
        </IconBox>
      </Box>
    </CustomFooter>
  );
};

export default Footer;
