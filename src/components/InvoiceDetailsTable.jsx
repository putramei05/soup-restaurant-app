import * as React from 'react';
import { styled } from '@mui/material/styles';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { Button } from '@mui/material';

const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
        backgroundColor: '#5B4947',
        color: theme.palette.common.white,
    },
    [`&.${tableCellClasses.body}`]: {
        fontSize: 14,
    },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
    '&:nth-of-type(even)': {
        backgroundColor: 'rgba(91, 73, 71, 0.2)',
    },
    // hide last border
    '&:last-child td, &:last-child th': {
        border: 0,
    },
}));

function createData(id, course_name, course_type, schedule, price) {
    return { id, course_name, course_type, schedule, price };
}

const rows = [
    createData(1, 'Tom Yum Thailand', 'Asian', 'Wednesday, 27 July 2022', 'IDR 450.000')
];

export default function InvoiceDetailsTable({item}) {
    // console.log(item)
    return (
        <TableContainer component={Paper}>
            <Table sx={{ minWidth: 700 }} aria-label="customized table">
                <TableHead sx={{
                    width: '1140px',
                    height: '60px',
                    padding: '20px, 20px, 20px, 0px',
                    gap: '24px'
                }}>
                    <TableRow>
                        <StyledTableCell sx={{
                            fontWeight: 600,
                            fontFamily: 'Montserrat',
                            fontSize: '16px',
                            lineHeight: '19.5px',
                            color: '#FFFFFF'
                        }}>No</StyledTableCell>
                        <StyledTableCell align="center" sx={{
                            fontWeight: 600,
                            fontFamily: 'Montserrat',
                            fontSize: '16px',
                            lineHeight: '19.5px',
                            color: '#FFFFFF'
                        }}>Course name</StyledTableCell>
                        <StyledTableCell align="center" sx={{
                            fontWeight: 600,
                            fontFamily: 'Montserrat',
                            fontSize: '16px',
                            lineHeight: '19.5px',
                            color: '#FFFFFF'
                        }}>Type</StyledTableCell>
                        <StyledTableCell align="center" sx={{
                            fontWeight: 600,
                            fontFamily: 'Montserrat',
                            fontSize: '16px',
                            lineHeight: '19.5px',
                            color: '#FFFFFF'
                        }}>Schedule</StyledTableCell>
                        <StyledTableCell align="center" sx={{
                            fontWeight: 600,
                            fontFamily: 'Montserrat',
                            fontSize: '16px',
                            lineHeight: '19.5px',
                            color: '#FFFFFF'
                        }}>Price</StyledTableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {item.map((row, index) => (
                        <StyledTableRow key={row.courseId}>
                            <StyledTableCell component="th" scope="row" sx={{
                                fontWeight: 500,
                                fontFamily: 'Montserrat',
                                fontSize: '16px',
                                lineHeight: '19.5px',
                                color: '#5B4947'
                            }}>
                                {index + 1}
                            </StyledTableCell>
                            <StyledTableCell align="center" sx={{
                                fontWeight: 500,
                                fontFamily: 'Montserrat',
                                fontSize: '16px',
                                lineHeight: '19.5px',
                                color: '#5B4947'
                            }}>{row.courseName}</StyledTableCell>
                            <StyledTableCell align="center" sx={{
                                fontWeight: 500,
                                fontFamily: 'Montserrat',
                                fontSize: '16px',
                                lineHeight: '19.5px',
                                color: '#5B4947'
                            }}>{row.typeName}</StyledTableCell>
                            <StyledTableCell align="center" sx={{
                                fontWeight: 500,
                                fontFamily: 'Montserrat',
                                fontSize: '16px',
                                lineHeight: '19.5px',
                                color: '#5B4947'
                            }}>{row.schedule}</StyledTableCell>
                            <StyledTableCell align="center" sx={{
                                fontWeight: 500,
                                fontFamily: 'Montserrat',
                                fontSize: '16px',
                                lineHeight: '19.5px',
                                color: '#5B4947'
                            }}>{row.coursePrice}</StyledTableCell>
                        </StyledTableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    );
}