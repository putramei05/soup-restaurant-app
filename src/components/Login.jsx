import { Box, Button, Container, TextField, Typography } from "@mui/material";
import { Link, useNavigate } from "react-router-dom";
import React, { useState } from "react";
import useLogin from "../hooks/useLogin";
import { useAuth } from "../contexts/AuthContext";

const Login = () => {
  const { login } = useAuth();
  const navigate = useNavigate();
  const {
    loginButtonFunc,
    setUserName,
    setPassword,
    errorMessage,
    userName,
    password,
  } = useLogin();

  // States for managing error status
  const [usernameError, setUsernameError] = useState(false);
  const [passwordError, setPasswordError] = useState(false);

  const handleLogin = () => {
    // Perform login validation here
    if (!userName && !password) {
      // If either username or password is empty, set the error states
      setPasswordError(true);
      setUsernameError(true);
    } else if (!password) {
      setPasswordError(true);
    } else if (!userName) {
      setUsernameError(true);
    } else {
      // Call your login function here
      loginButtonFunc();
    }
  };

  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
        minHeight: "80vh",
      }}
    >
      <Container maxWidth="sm">
        <Box
          sx={{
            display: "flex",
            flexDirection: "column",
            width: "640px",
            height: "399px",
          }}
        >
          <Typography
            sx={{
              fontSize: "24px",
              fontWeight: "500",
              color: "#5B4947",
              marginBottom: "16px",
            }}
          >
            Welcome Back Cheff
          </Typography>
          <Typography
            sx={{
              fontWeight: "400",
              color: "#4F4F4F",
              marginBottom: "40px",
            }}
          >
            Please login first
          </Typography>
          <Box sx={{ width: "auto", display: "flex", flexDirection: "column" }}>
            <TextField
              sx={{ marginBottom: "24px" }}
              id="outlined-basic"
              label="Username"
              variant="outlined"
              onChange={(e) => {
                setUserName(e.target.value);
                setUsernameError(false);
              }}
              error={usernameError}
            />
            <TextField
              sx={{ marginBottom: "24px" }}
              id="outlined-basic"
              label="Password"
              variant="outlined"
              type="password"
              onChange={(e) => {
                setPassword(e.target.value);
                setPasswordError(false);
              }}
              error={passwordError}
            />
            {errorMessage !== "" && (
              <Typography sx={{ color: "red" }}>{errorMessage}</Typography>
            )}
          </Box>
          <Box
            sx={{
              display: "flex",
              alignItems: "center",
            }}
          >
            <Typography
              sx={{ marginX: "3px", color: "#4F4F4F", cursor: "pointer" }}
            >
              Forgot Password?{" "}
            </Typography>
            <Typography
              component={Link}
              to="/reset"
              sx={{
                marginX: "3px",
                color: "#2F80ED",
                cursor: "pointer",
                textDecoration: "none",
              }}
            >
              Click Me
            </Typography>
          </Box>
          <Box
            sx={{
              display: "flex",
              alignItems: "center",
              justifyContent: "flex-end",
            }}
          >
            <Button
              sx={{
                marginTop: "20px",
                padding: "10px",
                background: "#FABC1D",
                color: "#5B4947",
                borderRadius: "8px",
                width: { xs: "100%", md: "120px" },
              }}
              onClick={() => handleLogin()}
            >
              Login
            </Button>
          </Box>
          <Box
            sx={{
              display: "flex",
              alignItems: "center",
              width: "100%",
              justifyContent: "center",
              marginTop: "60px",
            }}
          >
            <Typography
              sx={{ marginX: "3px", color: "#4F4F4F", cursor: "pointer" }}
            >
              Don't have account?
            </Typography>
            <Typography
              component={Link}
              to="/register"
              sx={{
                marginX: "3px",
                color: "#2F80ED",
                cursor: "pointer",
                textDecoration: "none",
              }}
            >
              Sign Up here
            </Typography>
          </Box>
        </Box>
      </Container>
    </Box>
  );
};

export default Login;
