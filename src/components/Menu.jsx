import {
  Box,
  Card,
  CardActionArea,
  CardActions,
  CardContent,
  CardMedia,
  Typography,
} from "@mui/material";
import React from "react";
import Footer from "./Footer";
import useFetchData from "../hooks/useFetchData";
import {Link} from "react-router-dom"

const Menu = ({ typeName }) => {
  const { data: data1, error: error1 } = useFetchData(`${process.env.REACT_APP_BASE_URL}/api/course/coursetype?name=${typeName}`);

  // console.log(data1, 'course type');
  return (
    <Box
      sx={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        flexDirection: "column",
      }}
    >
      <Typography
        sx={{
          display: "flex",
          color: "#5B4947",
          fontWeight: "600",
          fontSize: "32px",
          marginBottom: "80px",
        }}
      >
        Another menu in this class
      </Typography>

      <Box
        sx={{
          display: "flex",
        }}
      >
        {data1.map((item) => {
          return (
            <Box key={item.courseId} >
              <Card sx={{ maxWidth: 350, marginRight: "24px" }}>
                <CardActionArea component={Link} to={`/detailcourse/${item.courseId}`} >
                  <CardMedia
                    component="img"
                    height="233.333px"
                    image={item.courseImage}
                    alt="green iguana"
  
                  />
                  <CardContent>
                    <Typography variant="body2" color="text.secondary">
                      {item.fkCourseType}
                    </Typography>
                    <Typography
                      gutterBottom
                      variant="h5"
                      component="div"
                      sx={{ color: "#5B4947", fontWeight: "600" }}
                    >
                      {item.courseName}
                    </Typography>
                  </CardContent>
                </CardActionArea>
                <CardActions sx={{ marginLeft: "10px" }}>
                  <Typography
                    sx={{ color: "#FABC1D", fontWeight: "600", fontSize: "20px" }}
                  >
                    IDR {item.coursePrice}
                  </Typography>
                </CardActions>
              </Card>
            </Box>
          )
        })}
      </Box>
      <Footer />
    </Box>
  );
};

export default Menu;
