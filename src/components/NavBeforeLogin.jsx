import {
    AppBar,
    Box,
    Button,
    IconButton,
    Menu,
    MenuItem,
    Toolbar,
} from "@mui/material";
import { Outlet, Link } from "react-router-dom";
import React from "react";
import DehazeSharpIcon from "@mui/icons-material/DehazeSharp";
import "../styles/App.css";
import useLogin from "../hooks/useLogin";

const NavBeforeLogin = () => {
    const [auth, setAuth] = React.useState(true);
    const [anchorEl, setAnchorEl] = React.useState(null);

    const handleChange = (event) => {
        setAuth(event.target.checked);
    };

    const handleMenu = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };
    return (
    <Box>
        <Box
            sx={{
                alignItems: "center",
                justifyContent: "flex-end",
                display: { xs: "none", md: "flex" },
                mr: 1,
            }}
        >
            <Button
                component={Link}
                to="/login"
                sx={{
                    width: "175px",
                    padding: "10px 20px",
                    fontWeight: "500",
                    color: "#5B4947",
                    background: "white",
                    borderRadius: "8px",
                    border: "1px solid black",
                    marginRight: "20px",
                }}
            >
                Login
            </Button>

            <Button
                component={Link}
                to="/register"
                sx={{
                    width: "175px",
                    padding: "10px 20px",
                    fontWeight: "500",
                    color: "#5B4947",
                    background: "#FABC1D",
                    borderRadius: "8px",
                }}
            >
                Register
            </Button>
        </Box>
        <Box sx={{ flexGrow: 1, display: { xs: "flex", md: "none" } }}>
            <IconButton
                size="large"
                aria-label="account of current user"
                aria-controls="menu-appbar"
                aria-haspopup="true"
                onClick={handleMenu}
                color="inherit"
            >
                <DehazeSharpIcon sx={{ color: "#FABC1D" }} />
            </IconButton>

            <Menu
                id="menu-appbar"
                anchorEl={anchorEl}
                anchorOrigin={{
                    vertical: "top",
                    horizontal: "right",
                }}
                keepMounted
                transformOrigin={{
                    vertical: "top",
                    horizontal: "right",
                }}
                open={Boolean(anchorEl)}
                onClose={handleClose}
            >
                <MenuItem component={Link} to="/Login" onClick={handleClose}>
                    Login
                </MenuItem>
                <MenuItem component={Link} to="/Register" onClick={handleClose}>
                    Register
                </MenuItem>
            </Menu>
        </Box>
    </Box>
    )
}

export default NavBeforeLogin;