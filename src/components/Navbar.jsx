import React, { useState } from "react";
import {
  AppBar,
  Box,
  Button,
  IconButton,
  Menu,
  MenuItem,
  Toolbar,
  Typography,
} from "@mui/material";
import DehazeSharpIcon from "@mui/icons-material/DehazeSharp";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
import PersonIcon from "@mui/icons-material/Person";
import LogoutIcon from "@mui/icons-material/Logout";
import { Link, Outlet } from "react-router-dom";
import useLogout from "../hooks/useLogout";
import { useAuth } from "../contexts/AuthContext";

const Navbar = () => {
  const { authenticated } = useAuth();
  const [anchorEl, setAnchorEl] = useState(null);

  const { logoutButtonFunc } = useLogout();

  const handleMenu = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleLogout = () => {
    logoutButtonFunc();
  };

  return (
    <Box
      sx={{
        flexGrow: 1,
      }}
    >
      <AppBar position="static" elevation={0} sx={{ backgroundColor: "white" }}>
        <Toolbar
          sx={{
            width: "100%",
            padding: "1rem",
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "space-between",
            background: "white",
          }}
        >
          <Box component={Link} to="/">
            <img src="/images/logo.png" alt="" width={53.57} height={50} />
          </Box>
          <Box>
            <Box
              sx={{
                display: { xs: "none", md: "flex" },
                mr: 1,
              }}
            >
              {authenticated ? (
                <>
                  <Box component={Link} to="/Checkout">
                    <ShoppingCartIcon
                      sx={{
                        marginRight: "40px",
                        color: "#5B4947",
                        textDecoration: "none",
                      }}
                    />
                  </Box>
                  <Typography
                    component={Link}
                    to="/MyClass"
                    sx={{
                      fontFamily: "montserrat",
                      marginRight: "40px",
                      fontWeight: "500",
                      color: "#5B4947",
                      textDecoration: "none",
                    }}
                  >
                    My Class
                  </Typography>
                  <Typography
                    sx={{
                      marginRight: "40px",
                      fontWeight: "500",
                      color: "#5B4947",
                      textDecoration: "none",
                      fontFamily: "montserrat",
                    }}
                    component={Link}
                    to="/invoice"
                  >
                    Invoice
                  </Typography>
                  <Typography
                    sx={{
                      marginRight: "40px",
                      fontWeight: "500",
                      color: "#5B4947",
                    }}
                  >
                    |
                  </Typography>

                  <PersonIcon sx={{ color: "#FABC1D", marginRight: "50px" }} />
                  <LogoutIcon
                    sx={{ color: "#5B4947" }}
                    onClick={logoutButtonFunc}
                  />
                </>
              ) : (
                <>
                  <Button
                    component={Link}
                    to="/login"
                    sx={{
                      width: "175px",
                      padding: "10px 20px",
                      fontWeight: "500",
                      color: "#5B4947",
                      background: "white",
                      borderRadius: "8px",
                      border: "1px solid black",
                      marginRight: "20px",
                    }}
                  >
                    Login
                  </Button>

                  <Button
                    component={Link}
                    to="/register"
                    sx={{
                      width: "175px",
                      padding: "10px 20px",
                      fontWeight: "500",
                      color: "#5B4947",
                      background: "#FABC1D",
                      borderRadius: "8px",
                    }}
                  >
                    Register
                  </Button>
                </>
              )}
            </Box>
            <Box sx={{ flexGrow: 1, display: { xs: "flex", md: "none" } }}>
              <IconButton
                size="large"
                aria-label="account of current user"
                aria-controls="menu-appbar"
                aria-haspopup="true"
                onClick={handleMenu}
                color="inherit"
              >
                <DehazeSharpIcon sx={{ color: "#FABC1D" }} />
              </IconButton>

              <Menu
                id="menu-appbar"
                anchorEl={anchorEl}
                anchorOrigin={{
                  vertical: "top",
                  horizontal: "right",
                }}
                keepMounted
                transformOrigin={{
                  vertical: "top",
                  horizontal: "right",
                }}
                open={Boolean(anchorEl)}
                onClose={handleClose}
              >
                {authenticated ? (
                  <>
                    <MenuItem
                      component={Link}
                      to="/Checkout"
                      onClick={handleClose}
                    >
                      Keranjang
                    </MenuItem>
                    <MenuItem
                      component={Link}
                      to="/MyClass"
                      onClick={handleClose}
                    >
                      My Class
                    </MenuItem>
                    <MenuItem
                      component={Link}
                      to="/Invoice"
                      onClick={handleClose}
                    >
                      Invoice
                    </MenuItem>
                    <MenuItem onClick={handleClose}>Profile</MenuItem>
                    <MenuItem onClick={() => handleLogout()}>Log Out</MenuItem>
                  </>
                ) : (
                  <>
                    <MenuItem
                      component={Link}
                      to="/Login"
                      onClick={handleClose}
                    >
                      Login
                    </MenuItem>
                    <MenuItem
                      component={Link}
                      to="/Register"
                      onClick={handleClose}
                    >
                      Register
                    </MenuItem>
                  </>
                )}
              </Menu>
            </Box>
          </Box>
          <Outlet />
        </Toolbar>
      </AppBar>
    </Box>
  );
};

export default Navbar;
