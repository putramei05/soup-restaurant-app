import {
  Box,
  Button,
  IconButton,
  Menu,
  MenuItem,
  Typography,
  colors,
} from "@mui/material";
import {useEffect, useState} from "react";
import { Link, Outlet } from "react-router-dom";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
import PersonIcon from "@mui/icons-material/Person";
import LogoutIcon from "@mui/icons-material/Logout";
import DehazeSharpIcon from "@mui/icons-material/DehazeSharp";
import useLogout from "../hooks/useLogout";
import { getCookie } from "../utils/cookies";
import NavAfterLogin from "./NavAfterLogin";
import NavBeforeLogin from "./NavBeforeLogin";

const NavbarLogin = () => {
  const [auth, setAuth] = useState(true);
  const [anchorEl, setAnchorEl] = useState(null);
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  
  useEffect(() => {
    const token = getCookie("access_token"); // You need to extract the actual token here
    // console.log('Token:', token);
    setIsLoggedIn(!!token);
  }, []);

  const { logoutButtonFunc } = useLogout();

  const handleChange = (event) => {
    setAuth(event.target.checked);
  };

  const handleMenu = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };
  return (
    <Box
      sx={{
        width: "100%",
        padding: "1.5rem 2.5rem",
        display: "flex",
        alignItems: "center",
        justifyContent: "space-between",
        background: "white",
      }}
    >
      <Box component={Link} to="/">
        <img src="/images/logo.png" alt="" width={53.57} height={50} />
      </Box>
      
      {isLoggedIn ? <NavAfterLogin/> : <NavBeforeLogin/> }
      {/* <Box>
        <Box
          sx={{
            display: { xs: "none", md: "flex" },
            mr: 1,
          }}
        >
          <Box component={Link} to="/Checkout">
            <ShoppingCartIcon
              sx={{
                marginRight: "40px",
                color: "#5B4947",
                textDecoration: "none",
              }}
            />
          </Box>
          <Typography
            component={Link}
            to="/MyClass"
            sx={{
              fontFamily: "montserrat",
              marginRight: "40px",
              fontWeight: "500",
              color: "#5B4947",
              textDecoration: "none",
            }}
          >
            My Class
          </Typography>
          <Typography
            sx={{ marginRight: "40px", fontWeight: "500", color: "#5B4947", textDecoration: "none", fontFamily: "montserrat" }}
            component={Link} to="/invoice"
          >
            Invoice
          </Typography>
          <Typography
            sx={{ marginRight: "40px", fontWeight: "500", color: "#5B4947" }}
          >
            |
          </Typography>

          <PersonIcon sx={{ color: "#FABC1D", marginRight: "50px" }} />
          <LogoutIcon sx={{ color: "#5B4947" }} onClick={logoutButtonFunc} />
        </Box>
        <Box sx={{ flexGrow: 1, display: { xs: "flex", md: "none" } }}>
          <IconButton
            size="large"
            aria-label="account of current user"
            aria-controls="menu-appbar"
            aria-haspopup="true"
            onClick={handleMenu}
            color="inherit"
          >
            <DehazeSharpIcon sx={{ color: "#FABC1D" }} />
          </IconButton>

          <Menu
            id="menu-appbar"
            anchorEl={anchorEl}
            anchorOrigin={{
              vertical: "top",
              horizontal: "right",
            }}
            keepMounted
            transformOrigin={{
              vertical: "top",
              horizontal: "right",
            }}
            open={Boolean(anchorEl)}
            onClose={handleClose}
          >
            <MenuItem component={Link} to="/Checkout" onClick={handleClose}>
              Keranjang
            </MenuItem>
            <MenuItem component={Link} to="/MyClass" onClick={handleClose}>
              My Class
            </MenuItem>
            <MenuItem component={Link} to="/Invoice" onClick={handleClose}>
              Invoice
            </MenuItem>
            <MenuItem onClick={handleClose}>Profile</MenuItem>
            <MenuItem  onClick={logoutButtonFunc}>
              Log Out
            </MenuItem>
          </Menu>
        </Box>
      </Box> */}
      <Outlet />
    </Box>
  );
};

export default NavbarLogin;
