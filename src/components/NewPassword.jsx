import { Box, Button, TextField, Typography } from "@mui/material";
import React, { useState } from "react";
import { Link } from "react-router-dom";
import useResetPassword from "../hooks/useResetPassword";

const NewPassword = () => {
  const {
    resetButtonFunc,
    setPassword,
    setConfirmPassword,
    setResetToken,
    password,
    confirmPassword,
    errorMessage,
    resetToken,
  } = useResetPassword();

  const [resetTokenError, setResetTokenError] = useState(false);
  const [passwordError, setPasswordError] = useState(false);

  const handleReset = () => {
    // Perform register validation here
    if (resetToken === "" && password === "") {
      // If either username or password is empty, set the error states
      setPasswordError(true);
      setResetTokenError(true);
    } else if (password === "") {
      setPasswordError(true);
    } else if (resetToken === "") {
      setResetTokenError(true);
    } else {
      // Call your login function here
      resetButtonFunc();
    }
  };
  return (
    <Box
      sx={{
        display: "flex",
        justifyContent: "center",
      }}
    >
      <Box
        sx={{
          marginTop: "60px",
          display: "flex",
          flexDirection: "column",
          width: "640px",
          height: "399px",
          justifyContent: "center",
        }}
      >
        <Typography
          sx={{
            fontSize: "24px",
            fontWeight: "500",
            color: "#5B4947",
            marginBottom: "16px",
          }}
        >
          Create Password
        </Typography>
        <TextField
          sx={{ marginTop: "60px", marginBottom: "12px" }}
          id="outlined-basic"
          label="Code OTP"
          variant="outlined"
          onChange={(e) => {
            setResetToken(e.target.value);
          }}
          error={resetTokenError}
        />
        <TextField
          sx={{ marginTop: "10px", marginBottom: "12px" }}
          id="outlined-basic"
          label="New Password"
          variant="outlined"
          type="password"
          onChange={(e) => {
            setPassword(e.target.value);
          }}
          error={passwordError}
        />
        <TextField
          sx={{ marginTop: "60px", marginTop: "12px" }}
          id="outlined-basic"
          label="Confirm New Password"
          variant="outlined"
          type="password"
          onChange={(e) => setConfirmPassword(e.target.value)}
        />
        {errorMessage !== "" && (
          <Typography sx={{ color: "red" }}>{errorMessage}</Typography>
        )}
        <Box
          sx={{
            display: "flex",
            alignItems: "center",
            justifyContent: "flex-end",
            marginTop: "40px",
          }}
        >
          <Button
            component={Link}
            to="/reset"
            sx={{
              padding: "10px",
              background: "white",
              border: "1px solid black",
              color: "#5B4947",
              borderRadius: "8px",
              width: "120px",
              marginX: "12px",
            }}
          >
            Cancel
          </Button>
          <Button
            sx={{
              padding: "10px",
              background: "#FABC1D",
              color: "#5B4947",
              borderRadius: "8px",
              width: "120px",
              marginLeft: "12px",
            }}
            onClick={() => handleReset()}
          >
            Submit
          </Button>
        </Box>
      </Box>
    </Box>
  );
};

export default NewPassword;
