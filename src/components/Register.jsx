import React, { useState } from "react";
import { Button, Typography, Box, FormControl, TextField } from "@mui/material";
import { Link } from "react-router-dom";
import useRegister from "../hooks/useRegister";

const Register = () => {
  const {
    registerButtonFunc,
    setUserName,
    setEmail,
    setPassword,
    setConfirmPassword,
    errorMessage,
    email,
    userName,
    password,
  } = useRegister();

  const [usernameError, setUsernameError] = useState(false);
  const [emailError, setEmailError] = useState(false);
  const [passwordError, setPasswordError] = useState(false);

  const handleRegister = () => {
    // Perform register validation here
    if (userName === "" && password === "" && email === "") {
      // If either username or password is empty, set the error states
      setPasswordError(true);
      setUsernameError(true);
      setEmailError(true);
    } else if (password === "") {
      setPasswordError(true);
    } else if (userName === "") {
      setUsernameError(true);
    } else if (email === "") {
      setEmailError(true);
    } else {
      // Call your login function here
      registerButtonFunc();
    }
  };
  return (
    <Box
      sx={{
        flexGrow: 1,
        height: "467px",
        width: "616px",
        mt: "60px",
        marginLeft: "auto",
        marginRight: "auto",
      }}
    >
      <div>
        <Typography
          sx={{
            fontFamily: "Montserrat",
            fontSize: "24px",
            fontWeight: 500,
            color: "#5B4947",
          }}
        >
          Are you ready become a professional cheff?
        </Typography>
      </div>
      <div>
        <Typography
          sx={{
            fontFamily: "Montserrat",
            fontSize: "16px",
            fontWeight: 400,
            color: "#4F4F4F",
          }}
        >
          Please register first
        </Typography>
      </div>
      <div>
        <FormControl fullWidth variant="standard" sx={{ mt: 2 }}>
          <TextField
            id="outlined-basic"
            label="Name"
            variant="outlined"
            size="small"
            margin="dense"
            onChange={(e) => {
              setUserName(e.target.value);
            }}
            error={usernameError}
          />
        </FormControl>
      </div>
      <div>
        <FormControl fullWidth variant="standard" sx={{ mt: 2 }}>
          <TextField
            id="outlined-basic"
            label="Email"
            variant="outlined"
            size="small"
            margin="dense"
            onChange={(e) => {
              setEmail(e.target.value);
            }}
            error={emailError}
          />
        </FormControl>
      </div>
      <div>
        <FormControl fullWidth variant="standard" sx={{ mt: 2 }}>
          <TextField
            id="outlined-basic"
            label="Password"
            variant="outlined"
            size="small"
            type="password"
            margin="dense"
            onChange={(e) => {
              setPassword(e.target.value);
            }}
            error={passwordError}
          />
        </FormControl>
      </div>
      <div>
        <FormControl fullWidth variant="standard" sx={{ mt: 2 }}>
          <TextField
            id="outlined-basic"
            label="Confirm Password"
            variant="outlined"
            size="small"
            type="password"
            margin="dense"
            onChange={(e) => setConfirmPassword(e.target.value)}
          />
        </FormControl>
        {errorMessage !== "" && (
          <Typography sx={{ color: "red" }}>{errorMessage}</Typography>
        )}
      </div>

      <div style={{ display: "flex", justifyContent: "flex-end" }}>
        <Button
          variant="contained"
          // component={Link}
          // to="/success"
          sx={{
            mt: 2,
            backgroundColor: "#FABC1D",
            color: "#5B4947",
            borderRadius: "8px",
            width: "140px",
          }}
          onClick={() => handleRegister()}
        >
          Sign Up
        </Button>
      </div>
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          marginTop: "60px",
        }}
      >
        <div>
          <Typography
            sx={{
              fontFamily: "Montserrat",
              fontSize: "16px",
              fontWeight: 400,
              color: "#5B4947",
            }}
          >
            Have account?
          </Typography>
        </div>
        <div>
          <Typography
            component={Link}
            to="/login"
            sx={{
              ml: 1,
              fontSize: "16px",
              fontWeight: 400,
              lineHeight: "24px",
              width: "140px",
              marginX: "3px",
              color: "#2F80ED",
              cursor: "pointer",
              textDecoration: "none",
            }}
          >
            Login here
          </Typography>
        </div>
      </div>
    </Box>
  );
};

export default Register;
