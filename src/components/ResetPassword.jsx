import { Box, Button, TextField, Typography } from "@mui/material";
import React, { useState } from "react";
import { Link } from "react-router-dom";
import useForgot from "../hooks/useForgot";

const ResetPassword = () => {
  const { setEmail, email, forgotButtonFunc, errorMessage } = useForgot();

  const [emailError, setEmailError] = useState(false);

  const handleForgot = () => {
    if (email === "") {
      setEmailError(true);
    } else {
      forgotButtonFunc();
    }
  };
  return (
    <Box
      sx={{
        display: "flex",
        justifyContent: "center",
      }}
    >
      <Box
        sx={{
          marginTop: "60px",
          display: "flex",
          flexDirection: "column",
          width: "640px",
          height: "399px",
          justifyContent: "center",
        }}
      >
        <Typography
          sx={{
            fontSize: "24px",
            fontWeight: "500",
            color: "#5B4947",
            marginBottom: "16px",
          }}
        >
          Reset Password
        </Typography>
        <Typography
          sx={{
            fontWeight: "500",
            color: "#5B4947",
            marginBottom: "16px",
          }}
        >
          Send OTP code to your email address
        </Typography>
        <TextField
          sx={{ marginTop: "60px" }}
          id="outlined-basic"
          label="Email"
          variant="outlined"
          onChange={(e) => {
            setEmail(e.target.value);
          }}
          error={emailError}
        />
        {errorMessage !== "" && (
          <Typography sx={{ color: "red" }}>{errorMessage}</Typography>
        )}
        <Box
          sx={{
            display: "flex",
            alignItems: "center",
            justifyContent: "flex-end",
            marginTop: "40px",
          }}
        >
          <Button
            component={Link}
            to="/login"
            sx={{
              padding: "10px",
              background: "white",
              border: "1px solid black",
              color: "#5B4947",
              borderRadius: "8px",
              width: "120px",
              marginX: "12px",
            }}
          >
            Cancel
          </Button>
          <Button
            // component={Link}
            // to="/confirmpassword"
            sx={{
              padding: "10px",
              background: "#FABC1D",
              color: "#5B4947",
              borderRadius: "8px",
              width: "120px",
              marginLeft: "12px",
            }}
            onClick={() => handleForgot()}
          >
            Confirm
          </Button>
        </Box>
      </Box>
    </Box>
  );
};

export default ResetPassword;
