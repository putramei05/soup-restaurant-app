import React from 'react';
import {
    Button,
    Typography,
    Box
} from "@mui/material";
import { Link } from "react-router-dom"

const SuccessInvoice = () => {
    return (
        <Box sx={{
            flexGrow: 1,
            height: '437px',
            width: '370px',
            // border: '1px solid black',
            mt: '60px',
            marginLeft: 'auto',
            marginRight: 'auto'
        }}>
            <Box sx={{ display: 'flex', justifyContent: 'center' }}>
                <img src="/images/success.png" alt="" width={250} height={250} />
            </Box>
            <Box sx={{ display: 'flex', justifyContent: 'center' }}>
                <Typography sx={{ 
                    fontFamily: 'Montserrat', 
                    fontSize: '24px', 
                    fontWeight: 500, 
                    lineHeight: '29.26px', 
                    color: '#5B4947', 
                    mt: '40px' }}
                >
                        Purchase Successfully
                </Typography>
            </Box>
            <Box sx={{ display: 'flex', textAlign: 'center' }}>
                <Typography sx={{ 
                    fontFamily: 'Montserrat', 
                    fontSize: '16px', 
                    fontWeight: 400, 
                    lineHeight: '19.5px', 
                    color: '#4F4F4F', 
                    mt: '8px' }}
                >
                    Horay!! Let’s cook and become a professional cheff
                </Typography>
            </Box>
            <Box sx={{ display: 'flex', justifyContent: 'space-between' }}>
                <Button 
                    component={Link} to="/" 
                    variant="outlined"
                     
                    sx={{
                        fontFamily: 'Montserrat',
                        fontSize: '15px',
                        fontWeight: 600,
                        lineHeight: '18.29px',  
                        color: '#5B4947',
                        borderColor: '#5B4947',                     
                        width: '182px', 
                        mt: '40px', 
                        textTransform: 'capitalize' }}
                >
                    Back To Home
                </Button>
                <Button 
                    component={Link} to="/invoice" variant="text" 
                    sx={{
                        fontFamily: 'Montserrat',
                        fontSize: '15px',
                        fontWeight: 600,
                        lineHeight: '18.29px', 
                        backgroundColor: '#FABC1D', 
                        color: '#5B4947', 
                        width: '182px', 
                        mt: '40px', 
                        textTransform: 'capitalize' }}
                >
                    Open Invoice
                </Button>
            </Box>

        </Box>
    )
};

export default SuccessInvoice;