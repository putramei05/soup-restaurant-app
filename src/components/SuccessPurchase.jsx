import React from 'react';
import {
    Button,
    Typography,
    Box
} from "@mui/material";
import {Link} from "react-router-dom"

const SuccessPurchase = () => {
    return (
        <Box sx={{
            flexGrow: 1,
            height: '437px',
            width: '370px',
            // border: '1px solid black',
            mt: '60px',
            marginLeft: 'auto',
            marginRight: 'auto'    
        }}>
            <Box sx={{ display: 'flex', justifyContent: 'center'}}><img src="/images/success.png" alt="" width={250} height={250} /></Box>
            <Box sx={{ display: 'flex', justifyContent: 'center' }}><Typography sx={{ fontFamily: 'Montserrat', fontSize: '24px', fontWeight: 500, lineHeight: '29.26px', color: '#5B4947', mt: '40px' }}>Email Confirmation Success</Typography></Box>
            <Box sx={{ display: 'flex', textAlign: 'center' }}><Typography sx={{ fontFamily: 'Montserrat', fontSize: '16px', fontWeight: 400, lineHeight: '19.5px', color: '#4F4F4F', mt: '8px' }}>Congratulations! your email has already used.</Typography></Box>
            <Box sx={{ display: 'flex', justifyContent: 'center' }}><Button component={Link} to="/login" variant="text" sx={{ backgroundColor: '#FABC1D', color: '#5B4947', width: '140px', mt: '40px', textDecoration: 'none'  }}>Login Here</Button></Box>

        </Box>
    )
};

export default SuccessPurchase;