import { createContext, useContext, useState } from "react";

const CartContext = createContext();

export const useCart = () => {
    return useContext(CartContext);
};

export const CartProvider = ({ children }) => {
    const [cartItems, setCartItems] = useState([]);

    const addToCart = (item) => {
        setCartItems([...cartItems, item]);
    };

    const removeFromCart = (itemId) => {
        const updatedCartItems = cartItems.filter((item) => item.courseId !== itemId);
        setCartItems(updatedCartItems);
    };

    const cart = {
        items: cartItems,
        addToCart,
        removeFromCart
    };

    return (
        <CartContext.Provider value={cart}>{children}</CartContext.Provider>
    );
};
