import { useState } from 'react';
import axios from 'axios';

const useAddUser = () => {
    const [response, setResponse] = useState(null);
    const [error, setError] = useState(null);
    const [loading, setLoading] = useState(false);

    const submitUser = async (userData) => {
        try {
            setLoading(true);
            const response = await axios.post(`${process.env.REACT_APP_BASE_URL}/api/admin/adduser`, userData);
            setResponse(response.data);
        } catch (error) {
            setError(error);
        } finally {
            setLoading(false);
        }
    };

    return { response, error, loading, submitUser };
};

export default useAddUser;
