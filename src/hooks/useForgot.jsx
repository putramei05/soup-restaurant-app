import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import axios from "axios";

const useForgot = () => {
  const navigate = useNavigate();
  const [email, setEmail] = useState("");
  const [errorMessage, setErrorMessage] = useState("");

  const forgotButtonFunc = () => {
    axios
      .post(
        `${process.env.REACT_APP_BASE_URL}/api/user/ForgotPassword`,
        {
          email: email,
        },
        {
          headers: {
            "Content-Type": "application/json",
          },
        }
      )
      .then((res) => {
        alert(res.data);
        navigate("/confirmpassword");
      })
      .catch((err) => {
        // setErrorMessage(err.response.data);
        setErrorMessage(err.response.data);
      });
  };
  return {
    forgotButtonFunc,
    setEmail,
    errorMessage,
    email,
  };
};

export default useForgot;
