import { useState } from 'react';
import axios from 'axios';

const useInvoiceSubmit = () => {
    const [response, setResponse] = useState(null);
    const [error, setError] = useState(null);
    const [loading, setLoading] = useState(false);

    const submitInvoice = async (invoiceData) => {
        try {
            setLoading(true);
            const response = await axios.post(`${process.env.REACT_APP_BASE_URL}/api/checkout/addinvoice`, invoiceData);
            setResponse(response.data);
        } catch (error) {
            setError(error);
        } finally {
            setLoading(false);
        }
    };

    return { response, error, loading, submitInvoice };
};

export default useInvoiceSubmit;
