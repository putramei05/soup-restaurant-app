import React, { useEffect, useState } from "react";
import { useNavigate, Navigate } from "react-router-dom";
import axios from "axios";
import { useAuth } from "../contexts/AuthContext";

const useLogin = () => {
  const { login } = useAuth();
  const navigate = useNavigate();
  const [userName, setUserName] = useState("");
  const [password, setPassword] = useState("");
  const [errorMessage, setErrorMessage] = useState("");

  const loginButtonFunc = () => {
    axios
      .post(
        `${process.env.REACT_APP_BASE_URL}/api/login/user`,
        {
          userName: userName,
          password: password,
        },
        {
          headers: {
            "Content-Type": "application/json",
          },
        }
      )
      .then((res) => {
        document.cookie = `access_token=${res.data.token}`;
        document.cookie = `userId=${res.data.userId}`;
        document.cookie = `role=${res.data.role}`;
        login();

        if (res.data.role === 1) {
          navigate("/dashboardadmin");
        } else {
          navigate("/");
        }
        console.log(res);
      })
      .catch((err) => {
        setErrorMessage(err.response.data);
      });
  };

  return {
    loginButtonFunc,
    setUserName,
    setPassword,
    errorMessage,
    userName,
    password,
  };
};

export default useLogin;
