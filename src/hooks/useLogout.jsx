import { useNavigate } from "react-router-dom";
import { useAuth } from "../contexts/AuthContext";

const useLogout = () => {
  const { logout } = useAuth();
  const navigate = useNavigate();

  const logoutButtonFunc = () => {
    document.cookie =
      "access_token=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
    document.cookie = "userId=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
    document.cookie = "role=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
    navigate("/login"); // Redirect to login page after logout
    logout();
  };

  return {
    logoutButtonFunc,
  };
};

export default useLogout;
