import React, { useState } from "react";
import axios from "axios";

const useRegister = () => {
  const [userName, setUserName] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [email, setEmail] = useState("");
  const [errorMessage, setErrorMessage] = useState("");

  const registerButtonFunc = () => {
    console.log(password === confirmPassword);
    if (password === confirmPassword) {
      axios
        .post(
          `${process.env.REACT_APP_BASE_URL}/api/user/Register`,
          {
            userName: userName,
            password: password,
            email: email,
          },
          {
            headers: {
              "Content-Type": "application/json",
            },
          }
        )
        .then((res) => {
          alert("silahkan cek email!!");
        })
        .catch((err) => {
          setErrorMessage(err.response.data);
        });
    } else {
      setErrorMessage("Password dan Confirm password tidak sama");
    }
  };

  return {
    registerButtonFunc,
    setUserName,
    setEmail,
    setPassword,
    setConfirmPassword,
    password,
    confirmPassword,
    setErrorMessage,
    errorMessage,
  };
};

export default useRegister;
