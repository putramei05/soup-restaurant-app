import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import axios from "axios";

const useResetPassword = () => {
  const navigate = useNavigate();
  const [resetToken, setResetToken] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");

  const [errorMessage, setErrorMessage] = useState("");

  const resetButtonFunc = () => {
    if (password === confirmPassword) {
      axios
        .post(
          `${process.env.REACT_APP_BASE_URL}/api/user/ResetPassword`,
          {
            resetToken: resetToken,
            password: password,
          },
          {
            headers: {
              "Content-Type": "application/json",
            },
          }
        )
        .then((res) => {
          alert(res.data);
          navigate("/login");
        })
        .catch((err) => {
          setErrorMessage(err.response.data);
        });
    } else {
      setErrorMessage("password tidak boleh berbeda");
    }
  };

  return {
    resetButtonFunc,
    setPassword,
    setConfirmPassword,
    setResetToken,
    password,
    confirmPassword,
    setErrorMessage,
    errorMessage,
    resetToken,
  };
};

export default useResetPassword;
