import React from 'react';
import { Box, Typography } from "@mui/material";
import InvoiceDetailsTable from '../../components/InvoiceDetailsTable';
import useFetchData from '../../hooks/useFetchData';
import { useParams, Link } from 'react-router-dom';



const InvoiceDetailContent = () => {
    const { id } = useParams();

    const { data: data1, error: error1 } = useFetchData(`${process.env.REACT_APP_BASE_URL}/api/invoice/detail?id=${id}`);

    // console.log(data1);
    return (

        <Box>
            {data1.map((item) => {
                const inputDate = new Date(item.invoiceDate);

                const options = { year: 'numeric', month: 'long', day: 'numeric' };
                const formattedDate = inputDate.toLocaleDateString('en-US', options);

                console.log(formattedDate);
                return (
                    <Box key={item.invoiceId} >
                        <Box sx={{
                            width: '1140px',
                            height: '391px',
                            marginTop: '46px',
                            marginBottom: '207px',
                            marginX: 'auto'
                        }}>
                            <Box display='flex'>
                                <Typography
                                    mr='5px'
                                    component={Link} to={'/dashboardadmin'}
                                    sx={{
                                        fontWeight: 600,
                                        fontSize: '16px',
                                        lineHeight: '19.5px',
                                        color: '#828282',
                                        textDecoration: 'none'
                                    }}
                                >
                                    Home &gt;
                                </Typography>
                                <Typography fontWeight="fontWeightBold" sx={{
                                    fontWeight: 600,
                                    fontSize: '16px',
                                    lineHeight: '19.5px',
                                    color: '#5B4947'
                                }}> Details Invoice</Typography>
                            </Box>
                            <Box mt='32px'>
                                <Typography mb='24px' sx={{
                                    fontWeight: 600,
                                    fontFamily: 'Montserrat',
                                    fontSize: '20px',
                                    lineHeight: '24.38px',
                                    color: '#4F4F4F'
                                }}>Details Invoice</Typography>
                            </Box>
                            <Box display="flex" flexDirection="column" width="1140px">
                                <Box sx={{
                                    display: "flex",
                                    flexDirection: "row",
                                    justifyContent: "space-between",
                                    width: "250px",
                                    height: "22px",
                                    gap: "24px"
                                }} >
                                    <Typography sx={{
                                        fontWeight: 500,
                                        fontFamily: 'Montserrat',
                                        fontSize: '18px',
                                        lineHeight: '21.94px',
                                        color: '#4F4F4F'
                                    }}>No. Invoice :</Typography>
                                    <Typography sx={{
                                        fontWeight: 500,
                                        fontFamily: 'Montserrat',
                                        fontSize: '18px',
                                        lineHeight: '21.94px',
                                        color: '#4F4F4F'
                                    }}>{item.invoiceName}</Typography>
                                </Box>
                                <Box display="flex" flexDirection="row" width="1140px" height="22px" mt="8px">
                                    <Box sx={{
                                        display: "flex",
                                        flexDirection: "row",
                                        justifyContent: "space-between",
                                        width: "250px",
                                        height: "22px",
                                        gap: "24px"
                                    }} >
                                        <Typography sx={{
                                            fontWeight: 500,
                                            fontFamily: 'Montserrat',
                                            fontSize: '18px',
                                            lineHeight: '21.94px',
                                            color: '#4F4F4F'
                                        }}>Date :</Typography>
                                        <Typography sx={{
                                            fontWeight: 500,
                                            fontFamily: 'Montserrat',
                                            fontSize: '18px',
                                            lineHeight: '21.94px',
                                            color: '#4F4F4F'
                                        }}>{formattedDate}</Typography>
                                    </Box>
                                    <Box width="890px" height="22px" gap="24px">
                                        <Typography display="flex" justifyContent="flex-end" sx={{
                                            fontWeight: 700,
                                            fontFamily: 'Montserrat',
                                            fontSize: '18px',
                                            lineHeight: '21.94px',
                                            color: '#4F4F4F'
                                        }}>Total Price<Box component="span" fontWeight="fontWeightBold" sx={{
                                            fontWeight: 700,
                                            fontFamily: 'Montserrat',
                                            fontSize: '18px',
                                            lineHeight: '21.94px',
                                            color: '#4F4F4F',
                                            marginLeft: '24px'
                                        }}> IDR {item.invoicePrice}</Box></Typography>
                                    </Box>
                                </Box>
                            </Box>
                            <Box mt="42px">
                                <InvoiceDetailsTable item={item.invoiceDetails} />
                            </Box>
                        </Box>
                    </Box>
                )
            })}
        </Box>
    )
};

export default InvoiceDetailContent;

