import React from 'react';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormControl from '@mui/material/FormControl';
import FormLabel from '@mui/material/FormLabel';
import { Stack, TextField } from '@mui/material';
import UserTableAdmin from '../../components/Admin/UserTableAdmin';
import useAddUser from '../../hooks/useAddUser';

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

function UserContent() {
    const [open, setOpen] = React.useState(false);
    const [name, setName] = React.useState('');
    const [email, setEmail] = React.useState('');
    const [password, setPassword] = React.useState('');
    const [role, setRole] = React.useState(1);
    const [status, setStatus] = React.useState(true);
    const [nameError, setNameError] = React.useState("");
    const [emailError, setEmailError] = React.useState("");
    const [passwordError, setPasswordError] = React.useState("");
    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);

    const handleNameChange = (e) => {
        setName(e.target.value);
        setNameError(e.target.value === "" ? "Name is required" : "");
    };

    const handleEmailChange = (e) => {
        setEmail(e.target.value);
        setEmailError(e.target.value === "" ? "Email is required" : "");
    };

    const handlePasswordChange = (e) => {
        setPassword(e.target.value);
        setPasswordError(e.target.value === "" ? "Password is required" : "");
    };

    const handleRole = (event) => {
        setRole(parseInt(event.target.value));
    };

    const handleStatus = (event) => {
        setStatus(event.target.value === 'true');
    };

    const userData = {
        userName: name,
        email: email,
        password: password,
        userRole: role,
        isActive: status
    };

    // console.log(userData);

    const { submitUser } = useAddUser();

    const handleSubmit = (event) => {
        event.preventDefault();

        if (name !== "" && email !== "" && password !== "" && role !== "" && status !== "") {
            submitUser(userData);
            handleClose();
            window.alert("User added successfully!");
        } else {
            setNameError(name === "" ? "Name is required" : "");
            setEmailError(email === "" ? "Email is required" : "");
            setPasswordError(password === "" ? "Password is required" : "");
        }
    };


    return (
        <Box>
            <h2>User Page</h2>
            <Button 
                variant='contained' 
                onClick={handleOpen} 
                sx={{ 
                    marginTop: '10px', 
                    marginBottom: '20px', 
                    background: '#FABC1D', 
                    color: "#5B4947", 
                    fontFamily: 'Montserrat', 
                    textTransform: 'capitalize',
                    fontWeight: '700' 
                }}
            >
                Add User
            </Button>
            <Modal open={open} onClose={handleClose}>
                <Box sx={style}>
                    <FormControl onSubmit={handleSubmit}>
                        <Stack spacing={2}>
                            <Typography variant="h6" component="h2">
                                Add User
                            </Typography>
                            <TextField
                                label='User Name'
                                size="small"
                                variant="outlined"
                                value={name}
                                error={nameError !== ""}
                                helperText={nameError}
                                onChange={handleNameChange}
                            />
                            <TextField
                                label='Email'
                                size="small"
                                variant="outlined"
                                value={email}
                                error={emailError !== ""}
                                helperText={emailError}
                                onChange={handleEmailChange}
                            />  
                            <TextField
                                label="Password"
                                type='password'
                                size="small"
                                variant="outlined"
                                value={password}
                                error={passwordError !== ""}
                                helperText={passwordError}
                                onChange={handlePasswordChange}
                            />
                            <FormControl component='fieldset'>
                                <FormLabel component='legend'>Role</FormLabel>
                                <RadioGroup
                                    row
                                    aria-labelledby="demo-row-radio-buttons-group-label"
                                    name="controlled-radio-buttons-group"
                                    value={role}
                                    onChange={handleRole}
                                >
                                    <FormControlLabel value={1} control={<Radio />} label="Admin" />
                                    <FormControlLabel value={2} control={<Radio />} label="User" />
                                </RadioGroup>
                            </FormControl>
                            <FormControl component='fieldset'>
                                <FormLabel component='legend'>Status</FormLabel>
                                <RadioGroup
                                    row
                                    aria-labelledby="demo-row-radio-buttons-group-label"
                                    name="row-radio-buttons-group"
                                    value={status}
                                    onChange={handleStatus}
                                >
                                    <FormControlLabel value={true} control={<Radio />} label="Active" />
                                    <FormControlLabel value={false} control={<Radio />} label="Inactive" />
                                </RadioGroup>
                            </FormControl>  
                            <Button variant='contained' color='success' onClick={handleSubmit}>
                                Add
                            </Button>
                        </Stack>
                    </FormControl>
                </Box>
            </Modal>
            <UserTableAdmin/>
        </Box>
    );
}

export default UserContent;
