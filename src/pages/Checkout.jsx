import {
  Box,
  Button,
  Checkbox,
  List,
  ListItem,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  Typography,
  Radio
} from "@mui/material";
import React from "react";
import DeleteForeverIcon from "@mui/icons-material/DeleteForever";
import Modal from "@mui/material/Modal";
import { useCart } from "../contexts/CartContext";
import useFetchData from "../hooks/useFetchData";
import useInvoiceSubmit from "../hooks/useInvoiceSubmit";
import { Link } from "react-router-dom";
import { getCookie } from "../utils/cookies";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "2px solid #000",
  borderRadius: "10px",
  boxShadow: 24,
  p: 4,
};

const Checkout = ({ cartItems }) => {
  const [open, setOpen] = React.useState(false);
  const [selectedItems, setSelectedItems] = React.useState([]);
  const [checkboxStates, setCheckboxStates] = React.useState({});
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const userId = getCookie('userId');

  const { data: data1, error: error1 } = useFetchData(`${process.env.REACT_APP_BASE_URL}/api/checkout/payment`);

  const { items, addToCart, removeFromCart } = useCart();

  const [selectedPayment, setSelectedPayment] = React.useState([]);

  const handleItemClick = (paymentId) => () => {
    setSelectedPayment(paymentId);
  };

  // console.log(selectedPayment);

  const handleCheckboxChange = (itemId, checked) => {
    setSelectedItems((prevSelectedItems) => {
      if (checked) {
        setCheckboxStates((prevState) => ({
          ...prevState,
          [itemId]: true,
        }));
        const selectedItem = items.find((item) => item.courseId === itemId);
        if (selectedItem) {
          return [...prevSelectedItems, selectedItem];
        }
      } else {
        setCheckboxStates((prevState) => ({
          ...prevState,
          [itemId]: false,
        }));
        return prevSelectedItems.filter((item) => item.courseId !== itemId);
      }
      return prevSelectedItems;
    });
  };

  const isAllChecked = () => {
    const allIds = items.map((data) => data.courseId);
    return allIds.every((id) => checkboxStates[id]);
  };

  const handleSelectAllChange = (event) => {
    const checked = event.target.checked;

    const newCheckboxStates = {};

    items.forEach((data) => {
      newCheckboxStates[data.courseId] = checked;
    });

    setCheckboxStates(newCheckboxStates);
    if (checked) {
      setSelectedItems(items);
    } else {
      setSelectedItems([]);
    }
  };


  const handleDeleteItem = (itemId) => {
    removeFromCart(itemId);
    setSelectedItems((prevSelectedItems) =>
      prevSelectedItems.filter((item) => item.courseId !== itemId)
    );

    setCheckboxStates((prevState) => {
      const newState = { ...prevState };
      newState[itemId] = false;
      return newState;
    });
  };
  
  const totalSelectedPrice = selectedItems.reduce((total, selectedItem) => {
    return total + selectedItem.coursePrice;
  }, 0);
  
  let item = selectedItems;
  
  const invoiceData = {
    invoiceTotal: item.length,
    invoicePrice: totalSelectedPrice,
    fkIdUser: userId,
    payment:selectedPayment,
    invoiceDetails: selectedItems.map(item => ({
      courseId: item.courseId,
      schedule: item.schedule
    })) 
  };
  
  const { response, error, loading, submitInvoice } = useInvoiceSubmit();

  const handleSubmit = (event) => {
    // event.preventDefault();
    submitInvoice(invoiceData);
  };


  // console.log(selectedItems);
  console.log(items);

  return (
    <Box>
      
      <Box
        sx={{
          display: "flex",
          marginTop: "40px",
          marginX: "72px",
          alignItems: "center",
          marginBottom: "24px",
        }}
      >
        <Checkbox
          sx={{
            marginRight: "32px",
            width: "24px",
            height: "24px",
            color: "#5B4947",
          }}
          checked={isAllChecked()}
          onChange={handleSelectAllChange}
        />
        <Typography
          sx={{ fontSize: "20px", color: "#333", fontFamily: "Montserrat" }}
        >
          Pilih Semua
        </Typography>
      </Box>
      {items.map((data) => {
        return(
          <Box key = {data.courseId}>
            <Box
              sx={{
                border: "1px solid #BDBDBD",
                marginBottom: "24px",
                marginX: "72px",
              }}
            />
            <Box
              sx={{
                display: "flex",
                marginX: "72px",
                marginTop: "24px",
                alignItems: "center",
                alignSelf: "stretch",
                gap: "32px",
              }}
            >
              <Checkbox
                sx={{
                  width: "24px",
                  height: "24px",
                  color: "#5B4947",
                }}
                checked={checkboxStates[data.courseId] || false}
                onChange={(e) => handleCheckboxChange(data.courseId, e.target.checked)}
              />
              <img
                src={data.courseImage}
                alt=""
                style={{
                  width: "200px",
                  height: "133px",
                  marginRight: "24px",
                }}
              />
              <Box
                sx={{
                  display: "flex",
                  flexDirection: "column",
                  gap: "8px",
                  width: "100%",
                }}
              >
                <Typography
                  sx={{
                    color: "#828282",
                    fontFamily: "Montserrat",
                  }}
                >
                  {data.fkCourseType}
                </Typography>
                <Typography
                  sx={{
                    color: "#333",
                    fontFamily: "Montserrat",
                    fontSize: "24px",
                    fontWeight: "600",
                  }}
                >
                  {data.courseName}
                </Typography>
                <Typography
                  sx={{
                    color: "#4F4F4F",
                    fontFamily: "Montserrat",
                  }}
                >
                  Schedule : {data.schedule}
                </Typography>
                <Typography
                  sx={{
                    color: "#FABC1D",
                    fontFamily: "Montserrat",
                    fontSize: "20px",
                    fontWeight: "600",
                  }}
                >
                  IDR {data.coursePrice}
                </Typography>
              </Box>
              <DeleteForeverIcon
                sx={{ width: "40px", height: "40px", color: "#EB5757" }}
                onClick={() => handleDeleteItem(data.courseId)}
              />
            </Box>
          </Box>
        )
      })}
      
      <Box
        sx={{
          border: "1px solid #BDBDBD",
          height: "1px",
          marginTop: "24px",
          marginX: "72px",
        }}
      />
      <Box
        sx={{
          display: "inline-flex",
          padding: "30px 70px",
          alignItems: "center",
          justifyContent: "space-between",
          gap: "10px",
          marginTop: "290px",
          borderTop: "1px solid #BDBDBD",
          boxShadow: "0px -2px 3px 0px rgba(0, 0, 0, 0.25)",
          width: "100%",
        }}
      >
        <Box
          sx={{
            display: "flex",
            gap: "10px",
            alignItems: "center",
          }}
        >
          <Typography
            sx={{
              color: "#333",
              fontFamily: "Montserrat",
              fontSize: "18px",
              marginRight: "24px",
            }}
          >
            Total Price
          </Typography>
          <Typography
            sx={{
              color: "#FABC1D",
              fontFamily: "Montserrat",
              fontSize: "24px",
              fontWeight: "600",
              marginRight: "593px",
            }}
          >
            IDR {totalSelectedPrice}
          </Typography>
        </Box>
        <Button
          onClick={handleOpen}
          variant="contained"
          sx={{
            width: "233px",
            borderRadius: "8",
            background: "#FABC1D",
            color: "#5B4947",
          }}
        >
          Pay Now
        </Button>
        <Modal
          open={open}
          onClose={handleClose}
          aria-labelledby="modal-modal-title"
          aria-describedby="modal-modal-description"
        >
          <Box sx={style}>
            <Typography
              sx={{
                textAlign: "center",
                fontSize: "20px",
                fontWeight: "500",
                marginBottom: "24px",
              }}
            >
              Select Payment Method
            </Typography>
            <List>
              {data1.map((items) => (
                <ListItem key={items.paymentId} disablePadding>
                  <ListItemButton onClick={handleItemClick(items.paymentId)}>
                    <ListItemIcon>
                      <Radio
                        edge="start"
                        checked={selectedPayment === items.paymentId}
                        tabIndex={-1}
                        disableRipple
                      />
                    </ListItemIcon>
                    <img
                      src={items.paymentLogo}
                      alt=""
                      style={{ width: '40px', height: '40px' }}
                    />
                    <ListItemText  primary={items.paymentName} sx={{marginLeft: '15px'}}/>
                  </ListItemButton>
                </ListItem>
              ))}
            </List>
            <Box
              sx={{
                display: "flex",
                justifyContent: "space-around",
                marginTop: "32px",
              }}
            >
              <Button
                onClick={handleClose}
                sx={{
                  border: "1px solid #5B4947",
                  color: "#5B4947",
                  width: "123px",
                }}
              >
                Cancel
              </Button>
              <Button
                type="button"
                variant="contained"
                sx={{
                  marginLeft: "8px",
                  padding: "10px 20px",
                  width: "194px",
                  background: "#FABC1D",
                  color: "#5B4947",
                }}
              >
                <Link
                  to="/successinvoice"
                  style={{ textDecoration: "none", color: "#5B4947", width: "100%", display: "block" }}
                  onClick={(event) => {
                    if (selectedItems.length === 0) {
                      alert("Selected items are empty.");
                      event.preventDefault();
                      handleClose();
                    } else {
                      alert("Purchase Successful");
                      handleSubmit();
                    }
                  }}
                >
                  Buy Now
                </Link>
              </Button> 
            </Box>
          </Box>
        </Modal>
      </Box>
      
    </Box>
  );
};

export default Checkout;
