import { Typography } from "@mui/material";
import { Box } from "@mui/system";
import React from "react";
import { useParams } from "react-router-dom";
import Menu from "../components/Menu";
import useFetchData from "../hooks/useFetchData";

const Course = () => {
  const { id } = useParams();

  const { data: data1, error: error1 } = useFetchData(
    `${process.env.REACT_APP_BASE_URL}/api/course/type?id=${id}`
  );

  // console.log(data1);

  return (
    <Box>
      {data1.map((data) => {
        return (
          <Box>
            <Box
              sx={{
                display: "flex",
                width: "auto",
                height: "274px",
                border: "1px solid black",
                alignItems: "center",
                justifyContent: "center",
                alignContent: "center",
                backgroundImage: `url(${data.typeImage})`,
                backgroundSize: "cover",
                backgroundRepeat: "no-repeat",
              }}
            />
            <Box
              sx={{
                marginTop: "91px",
                marginBottom: "80px",
                marginX: "70px",
              }}
            >
              <Typography
                sx={{
                  marginBottom: "16px",
                  color: "#000",
                  fontSize: "24px",
                  fontWeight: "600",
                }}
              >
                {data.typeName}
              </Typography>
              <Typography>{data.typeDetail}</Typography>
            </Box>
            <Box
              sx={{
                height: "1px",
                background: "#E0E0E0",
                marginBottom: "52px",
              }}
            />
            <Menu typeName={data.typeName} />
          </Box>
        );
      })}
    </Box>
  );
};

export default Course;
