import {
  Box,
  Button,
  Typography
} from "@mui/material";
import dayjs from "dayjs";
import Menu from "../components/Menu";
import React, { useState, useEffect } from "react";
import useFetchData from "../hooks/useFetchData";
import {useParams} from "react-router-dom";
import { useCart } from "../contexts/CartContext";
import { DemoContainer } from '@mui/x-date-pickers/internals/demo';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import useInvoiceSubmit from "../hooks/useInvoiceSubmit";
import { getCookie } from "../utils/cookies";

const DetailsCourse = () => {
  const [schedule, setSchedule] = useState("");
  const [coursePrice, setCoursePrice] = useState(0);
  const userId = getCookie('userId');

  const { id } = useParams();
  const { data: data1, error: error1 } = useFetchData(`${process.env.REACT_APP_BASE_URL}/api/course/listcourse?id=${id}`);

  const { addToCart, items: cartItems } = useCart();

  useEffect(() => {
    if (data1 && data1.length > 0) {
      setCoursePrice(data1[0].coursePrice);
    }
  }, [data1]);

  const invoiceData = {
    invoiceTotal: 1,
    invoicePrice: coursePrice,
    fkIdUser: userId,
    payment: 1,
    invoiceDetails: [{
      courseId: id,
      schedule: schedule
    }]
  };

  const { response, error, loading, submitInvoice } = useInvoiceSubmit();

  const handleSubmit = (event) => {
    event.preventDefault();
    submitInvoice(invoiceData);
  };

  const today = dayjs();
  const tomorrow = dayjs().add(1, 'day');

  // console.log(schedule);
  // console.log(id);
  // console.log(invoiceData);
  return (
    <Box>
      {data1.map((item) => {
      return (
        <Box key={item.courseId} >
        <Box
          sx={{
            display: "flex",
            marginX: "70px",
            marginTop: "40px",
          }}
        >
          <img
            src= {item.courseImage}
            alt=""
            style={{
              width: "400px",
              marginRight: "40px",
            }}
          />
          <Box>
            <Typography sx={{ color: "#828282", fontFamily: "Montserrat" }}>
              {item.fkCourseType}
            </Typography>
            <Typography
              sx={{
                color: "#333",
                marginY: "8px",
                fontSize: "24px",
                fontWeight: "600",
                fontFamily: "Montserrat",
              }}
            >
              {item.courseName}
            </Typography>
            <Typography
              sx={{
                fontFamily: "Montserrat",
                color: "#5B4947",
                fontSize: "24px",
                fontWeight: "600",
                marginBottom: "32px",
              }}
            >
              IDR {item.coursePrice}
            </Typography>

              <LocalizationProvider dateAdapter={AdapterDayjs}>
                <DemoContainer components={['DatePicker']}>
                  <DatePicker 
                      label="Choose the schedule"
                      minDate={today}
                      onChange={(schedule) => setSchedule(schedule.$d.toDateString())}
                  />
                </DemoContainer>
              </LocalizationProvider>
            <Box sx={{ marginTop: "60px" }}>
              <Button
                variant="text"
                  onClick={() => {
                    if (!schedule) {
                      alert("Please select a schedule before adding to cart.");
                      return;
                    } else {
                      alert("Course added to cart")
                    }

                    addToCart({
                      courseId: item.courseId,
                      courseName: item.courseName,
                      coursePrice: item.coursePrice,
                      courseImage: item.courseImage,
                      fkCourseType: item.fkCourseType,
                      schedule: schedule
                    });
                  }}
                sx={{
                  marginRight: "8px",
                  padding: "10px 20px",
                  width: "194px",
                  border: "1px solid #5B4947",
                  borderRadius: "8px",
                  color: "#5B4947",
                }}
              >
                Add to Cart
              </Button>
              <Button
                variant="contained"
                  onClick={(event) => {
                    if (!schedule) {
                      alert("Please select a schedule before buy the course.");
                      return;
                    } else {
                      alert("Purchase Successfull");
                      handleSubmit(event);
                    }
                  }}
                sx={{
                  marginLeft: "8px",
                  padding: "10px 20px",
                  width: "194px",
                  background: "#FABC1D",
                  color: "#5B4947",
                }}
              >
                Buy Now
              </Button>
            </Box>
          </Box>
        </Box>
        <Box
          sx={{
            display: "flex",
            marginTop: "40px",
            marginX: "70px",
            flexDirection: "column",
          }}
        >
          <Typography
            sx={{
              color: "#333333",
              fontWeight: "600",
              fontSize: "24px",
              marginBottom: "16px",
            }}
          >
            Description
          </Typography>
          <Typography
            sx={{
              color: "#333",
              fontFamily: "Montserrat",
              marginBottom: "24px",
            }}
          >
            {item.courseDetail}
          </Typography>
        </Box>
        <Box
          sx={{
            height: "1px",
            border: "1px solid #E0E0E0",
            marginBottom: "80px",
          }}
        />
          <Menu typeName={item.fkCourseType} />
      </Box> 
        )
      })}
    </Box>
  );
};

export default DetailsCourse;
