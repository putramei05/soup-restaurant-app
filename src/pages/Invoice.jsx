import React from "react";
import { Box, Typography } from "@mui/material";
import Footer from "../components/Footer";
import InvoiceTable from "../components/InvoiceTable";

const Invoice = () => {
  return (
    <Box sx={{ width: "100%" }}>
      <Box
        sx={{
          width: "1140px",
          height: "auto",
          marginTop: "46px",
          marginBottom: "207px",
          marginX: "auto",
        }}
      >
        <Typography
          sx={{
            fontWeight: 600,
            fontSize: "16px",
            lineHeight: "19.5px",
            color: "#828282",
          }}
        >
          Home &gt;{" "}
          <Box
            component="span"
            fontWeight="fontWeightBold"
            sx={{
              fontWeight: 600,
              fontSize: "16px",
              lineHeight: "19.5px",
              color: "#5B4947",
            }}
          >
            Invoice
          </Box>
        </Typography>
        <Box mt="32px">
          <Typography
            mb="24px"
            sx={{
              fontWeight: 600,
              fontFamily: "Montserrat",
              fontSize: "20px",
              lineHeight: "24.38px",
              color: "#4F4F4F",
            }}
          >
            Menu Invoice
          </Typography>
          <InvoiceTable />
        </Box>
      </Box>
      <Footer />
    </Box>
  );
};

export default Invoice;
