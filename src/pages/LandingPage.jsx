import React from "react";
import Footer from "../components/Footer";
import {
  Box,
  Card,
  CardActionArea,
  CardActions,
  CardContent,
  CardMedia,
  Typography,
} from "@mui/material";
import styled from "@emotion/styled";
import useFetchData from "../hooks/useFetchData";
import {Link} from "react-router-dom"

const FoodContainer = styled(Box)`
  display: flex;
  align-items: center;
  justify-content: center;
  margin-top: 60px;
  @media (max-width: 768px) {
    flex-direction: column;
  }
`;
const FoodPreviewContainer = styled(Box)`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-wrap: wrap;
  margin-top: 60px;
`;

const CustomCardMedia = styled(CardMedia)`
  height: 233px;
  @media (max-width: 768px) {
    height: 250px;
  }
`;

const CustomCard = styled(Card)`
  width: 350px;
  margin: 24px;
  flex-direction: column;
  @media (max-width: 768px) {
    width: 100%;
    margin: 12px 0;
  }
`;
const FoodSection = styled(Card)`
  display: flex;
  align-items: center;
  flex-direction: column;
  justify-content: center;
  @media (max-width: 768px) {
    flex-direction: column;
    margin: 0;
  }
`;
const PrestigeCard = styled(Card)`
  padding: 16px;
  width: 292px;
  height: 175px;
  border: 1px solid var(--gray-4, #bdbdbd);
  box-shadow: 0px 2px 2px 0px rgba(0, 0, 0, 0.2);
  border-radius: 20px;
  margin: 20px;
`;

const LandingPage = () => {
  const { data: data1, error: error1 } = useFetchData(`${process.env.REACT_APP_BASE_URL}/api/course/listcourse`);
  const { data: data2, error: error2 } = useFetchData(`${process.env.REACT_APP_BASE_URL}/api/course/type`);

  // console.log(process.env.REACT_APP_BASE_URL, 'base url')

  const prestigeObj = [
    {
      prestigeNumber: "200+",
      text: "Various cuisines avaible in progressional class",
    },
    {
      prestigeNumber: "50+",
      text: "A chef who is reliable and has his own characteristic in cooking",
    },
    {
      prestigeNumber: "30+",
      text: "Cooperate with trusted and upscale restaurants",
    },
  ];

  const foodPreview = data1.map(course => {
    return {
      key: course.courseId,
      category: course.fkCourseType,
      name: course.courseName,
      price: course.coursePrice,
      image: course.courseImage
    };
  });

  const foodCategories = data2.map(type => {
    return {
      key: type.typeId,
      name: type.typeName,
      image: type.typeImage
    };
  });

  return (
    <Box
      sx={{
        width: "100%",
      }}
    >
      {/* Bagian 1 */}
      <Box
        sx={{
          display: "flex",
          width: "auto",
          height: "274px",
          border: "1px solid black",
          alignItems: "center",
          justifyContent: "center",
          alignContent: "center",
          backgroundImage: 'url("/images/banner-background.png")',
          backgroundSize: "cover",
          backgroundRepeat: "no-repeat",
        }}
      >
        <Box
          sx={{
            display: "flex",
            width: "843px",
            alignItems: "center",
            justifyContent: "center",
            alignContent: "center",
            flexDirection: "column",
          }}
        >
          <Typography
            sx={{
              marginY: "14px",
              fontWeight: "600",
              fontSize: "32px",
              color: "#fff",
            }}
          >
            Be the next great chef
          </Typography>
          <Typography
            sx={{
              marginTop: "14px",
              fontWeight: "400",
              fontSize: "24px",
              color: "#fff",
              textAlign: "center",
            }}
          >
            We are able to awaken your cooking skills to become a classy chef
            and ready to dive into the professional world
          </Typography>
        </Box>
      </Box>

      {/* Bagian 2 */}
      <FoodContainer>
        {prestigeObj.map((data, index) => {
          return (
            <PrestigeCard key={index}>
              <Typography
                sx={{
                  marginBottom: "16px",
                  fontWeight: "400",
                  fontSize: "48px",
                  color: "#FABC1D",
                  textAlign: "center",
                }}
              >
                {data.prestigeNumber}
              </Typography>
              <Typography
                sx={{
                  fontWeight: "400",
                  fontSize: "16px",
                  color: "##5B4947",
                  textAlign: "center",
                }}
              >
                {data.text}
              </Typography>
            </PrestigeCard>
          );
        })}
      </FoodContainer>
      {/* Card */}
      <Box
        sx={{
          marginTop: "56px",
          marginLeft: "91px",
          marginRight: "91px",
        }}
      >
        <Typography
          sx={{
            textAlign: "center",
            fontSize: "32px",
            fontWeight: "600",
            color: "#5B4947",
          }}
        >
          More professional class
        </Typography>

        <FoodPreviewContainer>
          {foodPreview.map((data) => {
            return (
              <CustomCard key={data.key}>
                <CardActionArea  component={Link} to={`/detailcourse/${data.key}`}>
                  <CardMedia
                    component="img"
                    image={data.image}
                    alt={data.name}
                  />
                  <CardContent>
                    <Typography variant="body2" color="text.secondary">
                      {data.category}
                    </Typography>
                    <Typography
                      gutterBottom
                      variant="h5"
                      component="div"
                      sx={{ color: "#5B4947", fontWeight: "600" }}
                    >
                      {data.name}
                    </Typography>
                  </CardContent>
                </CardActionArea>
                <CardActions >
                  <Typography sx={{ color: '#FABC1D', fontWeight: 600, fontSize: '20px', lineHeight: '24.38px'}}>
                    {data.price.toLocaleString('id-ID', {
                      style: 'currency',
                      currency: 'IDR',
                      minimumFractionDigits: 0,
                      maximumFractionDigits: 0
                    })}
                  </Typography>
                </CardActions>
              </CustomCard>
            );
          })}
        </FoodPreviewContainer>
      </Box>
      <Box
        sx={{
          marginTop: "130px",
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          paddingX: "60px",
          width: "100%",
          paddingY: "24px",
          border: "1px solid black",
          backgroundImage: 'url("/images/banner-background2.png")',
          backgroundSize: "cover",
          backgroundRepeat: "no-repeat",
        }}
      >
        <Typography
          sx={{
            color: "#FFF",
            fontWeight: "600",
            fontSize: "40px",
          }}
        >
          Gets your best benefit
        </Typography>
        <Typography
          sx={{
            color: "#FFF",
            marginTop: "24px",
            textAlign: "justify",
          }}
        >
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Rerum fugit
          ad omnis. Lorem ipsum, dolor sit amet consectetur adipisicing elit.
          Quod odio ipsum ut sapiente. Pariatur, nemo. Lorem ipsum dolor sit
          amet consectetur adipisicing elit. Unde neque quam totam nisi,
          doloribus autem blanditiis! Eligendi suscipit similique quibusdam odio
          facilis, atque, pariatur nihil a perferendis tempore consectetur.
          Enim, rem provident dicta, itaque reiciendis consectetur ipsam quas
          corporis sequi natus autem possimus hic quis necessitatibus blanditiis
          animi consequatur minima, asperiores modi porro minus laudantium
          libero? Cupiditate molestias natus incidunt, aspernatur suscipit ullam
          modi, animi consequatur assumenda cumque
        </Typography>
      </Box>
      {/* Bagian 5 */}
      <FoodSection>
        <Typography
          sx={{
            textAlign: "center",
            fontSize: "32px",
            fontWeight: "600",
            color: "#5B4947",
          }}
        >
          More food type as you can choose
        </Typography>
        <Box
          sx={{
            marginTop: "80px",
            width: "100%",
            display: "flex",
            flexDirection: "row",
            flexWrap: "wrap",
            justifyContent: "center",
          }}
        >
          {foodCategories.map((data) => {
            return (
              <CustomCard key={data.key}>
                <CardActionArea component={Link} to={`/course/${data.key}`}>
                  <CardMedia
                    component="img"
                    height="133.333p"
                    image={data.image}
                    alt={data.name}
                  />
                  <CardContent>
                    <Typography
                      sx={{
                        color: "#5B4947",
                        fontWeight: "600",
                        textAlign: "center",
                      }}
                    >
                      {data.name}
                    </Typography>
                  </CardContent>
                </CardActionArea>
              </CustomCard>
            );
          })}
        </Box>
      </FoodSection>
      {/* Footer */}
      <Footer />
    </Box>
  );
};

export default LandingPage;
