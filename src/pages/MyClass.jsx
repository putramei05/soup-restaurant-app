import { Box, Typography } from "@mui/material";
import React from "react";
import Footer from "../components/Footer";
import useFetchData from "../hooks/useFetchData";
import { getCookie } from "../utils/cookies";

const MyClass = () => {
  // const userId = 2;
  const userId = getCookie('userId');
  const { data: data1, error: error1 } = useFetchData(`${process.env.REACT_APP_BASE_URL}/api/myclass/get?id=${userId}`); 

  // console.log(data1);
  return (
    
    <Box>
      {data1.map((item, index) => {
        return (
          <Box key={index}>
              <Box
                sx={{
                  display: "flex",
                  alignItems: "center",
                  marginX: "71px",
                  marginTop: "40px",
                  marginBottom: "24px",
                }}
              >
                <img
                  src={item.courseImage}
                  alt=""
                  style={{
                    width: "200px",
                    marginRight: "40px",
                  }}
                />
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "column",
                  }}
                >
                  <Typography sx={{ color: "#828282", marginBottom: "4px" }}>
                    {item.typeName}
                  </Typography>
                  <Typography
                    sx={{
                      color: "#333",
                      fontSize: "24px",
                      fontWeight: "600",
                      marginBottom: "8px",
                    }}
                  >
                    {item.courseName}
                  </Typography>
                  <Typography
                    sx={{ fontSize: "20px", color: "#FABC1D", fontWeight: "500" }}
                  >
                    Schedule: {item.schedule}
                  </Typography>
                </Box>
              </Box>
              <Box
                sx={{
                  background: "#BDBDBD",
                  border: "1px solid #BDBDBD",
                  height: "1px",
                  width: "100%",
                  marginBottom: "10px",
                }}
              />
          </Box>
        )
      })}
      <Footer />
    </Box>
  );
};

export default MyClass;
